package de.grogra.rgg.io;

import de.grogra.graph.impl.GraphManager;
import de.grogra.graph.impl.Node;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.expr.Expression;
import de.grogra.pf.ui.Command;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.JobManager;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.rgg.RGGRoot;

public class ImportNode {
	
	public static void addNode (Item item, Object info, Context context)
	{
		final Node node;
		final Expression expr;
		if (info instanceof Node)
		{
			node = (Node) info;
			expr = null;
		}
		else if (info instanceof ActionEditEvent)
		{
			node = null;
			ActionEditEvent e = (ActionEditEvent) info;
			if (e.isConsumed ()
				|| !((info = e.getSource ()) instanceof Expression))
			{
				return;
			}
			e.consume ();
			expr = (Expression) info;
		}
		else
		{
			return;
		}
		final Workbench w = context.getWorkbench ();
		UI.executeLockedly (w.getRegistry ().getProjectGraph (), true,
			new Command ()
			{
				@Override
				public String getCommandName ()
				{
					return null;
				}

				@Override
				public void run (Object arg, Context c)
				{
					Object o = (node != null) ? node : expr.evaluate (w, UI
						.getArgs (c, expr));
					if (!(o instanceof Node))
					{
						return;
					}
					((Node) o).setExtentIndex (Node.LAST_EXTENT_INDEX-1);
					GraphManager g = w.getRegistry ().getProjectGraph ();
					RGGRoot.getRoot(g).addEdgeBitsTo (
							(Node) o,
							de.grogra.graph.Graph.BRANCH_EDGE,
							g.getActiveTransaction ());
				}
			}, null, context, JobManager.ACTION_FLAGS);
	}

}
