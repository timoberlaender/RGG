package de.grogra.rgg.io;

import de.grogra.imp.View;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.ProjectWorkbench;
import de.grogra.pf.ui.event.ActionEditEvent;
import de.grogra.rgg.RGGRoot;
import de.grogra.graph.impl.Node;

public class ExportNode {

	
	public static void exportRGGGraph(Item item, Object info, Context ctx) throws Exception {
		ActionEditEvent e = (ActionEditEvent)info;
		View view = (View) e.getPanel ();
		
		Node root = RGGRoot.getRoot( ctx.getWorkbench(). getRegistry().getProjectGraph()  );
		
		((ProjectWorkbench)ctx.getWorkbench()).exportFromNode(info, root);
	}
		
	
}
