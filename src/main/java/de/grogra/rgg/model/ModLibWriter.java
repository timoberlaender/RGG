
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.rgg.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.GraphXMLSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.io.PluginCollector;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.PluginClassLoader;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.Value;
import de.grogra.util.MimeType;

public class ModLibWriter extends FilterBase implements FileWriterSource
{
	public static final IOFlavor FLAVOR
	= new IOFlavor (new MimeType ("application/x-grogra-model-lib", null),
			IOFlavor.FILE_WRITER, null);


	public ModLibWriter (FilterItem item, FilterSource source)
	{
		super (item, source);
		setFlavor (FLAVOR);
	}
	
	String pkgName="";
	String pluginName="";
	Collection dependencies;

	@Override
	public void write(File dir) throws IOException {
		if (dir.exists() && !dir.isDirectory()) {
			return;
		}
		if (!dir.exists()) {
			dir.mkdir();
		}
		pluginName = dir.getName();
		pkgName = ((CompiledLib) ((ObjectSource) source).getObject ()).prefixes;
		dependencies = ((CompiledLib) ((ObjectSource) source).getObject ()).imports;
		// create the jar
		Registry r = ((CompiledLib) ((ObjectSource) source).getObject ()).registry;
		byte[] compiled = null;
		Item i = r.getItem("/export/compiled/rggc");
		if (i instanceof Value)
		{
			compiled = (byte[]) ((Value) i).getObject();
		}
		File outputFile = new File(dir, pluginName + ".jar");
		try (FileOutputStream outputStream = new FileOutputStream(outputFile)) {
			outputStream.write(compiled);
		}
		
		File resourceFile = new File(dir, pluginName + ".grz");
		// write resources
		writeResources(resourceFile, r);

	
		// write plugin.xml
		writePluginXML(dir, r);

		// write plugin.properties
		outputFile = new File(dir,"plugin.properties");
		try (PrintWriter outputStream = new PrintWriter(outputFile)) {
			outputStream.write("pluginName = "+pluginName);
		}

	}


	protected void writePluginXML(File dir, Registry r) throws FileNotFoundException {
		File outputFile = new File(dir,"plugin.xml");
		try (PrintWriter outputStream = new PrintWriter(outputFile)) {
			outputStream.println( buildHeader(pkgName + "." + pluginName, r));
			outputStream.println( buildImports(dir, r));
			outputStream.println( buildRegistry(dir, r));
			outputStream.println( buildFooter(dir, r));
		}
	}

	protected String buildHeader(String id, Registry r) {
		return "<?xml version = \"1.0\" encoding=\"UTF-8\"?>\n"
				+ "<plugin\n"
				+ "  id=\""+ id +"\"\n"
				+ "  version=\""+ Main.getVersion() + "\"\n"
				+ "  xmlns=\"http://grogra.de/registry\">\n";
	}

	protected String buildImports(File dir, Registry r) {
		StringBuilder sb = new StringBuilder();
		for (Iterator it = dependencies.iterator (); it.hasNext (); )
		{
			PluginDescriptor pd = (PluginDescriptor) it.next ();
			sb.append("<import plugin=\"" + pd.getName() +"\" />\n");
		}
		sb.append("  <library file=\"" + pluginName +".jar\" prefixes=\"{" + pkgName + "}\"/>\n");
		sb.append("  <resourcelibrary name=\"" + pluginName +"\" file=\"" + pluginName +".grz\" />\n");

		return sb.toString();
	}

	protected String buildRegistry(File dir, Registry r) {
		return "";
	}
	
	protected String buildFooter(File dir, Registry r) {
		return "</plugin>";
	}


	protected void writeResources(File file, Registry r) {

		//get the dependencies
		try {
			PluginCollector pc = new CompletePluginCollector (dependencies);
			IO.writeXML (new GraphXMLSource (r.getProjectGraph (), r, pc),
						 r.getFileSystem(), "g.xml", GraphXMLSource.MIME_TYPE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// write the .grz
		try {
			FilterSource fs = IO.createPipeline(
					new ObjectSourceImpl(r, "registry", MimeType.valueOf(Registry.class), r, null),
					new IOFlavor(new MimeType("application/x-grogra-resource+zip"), IOFlavor.FILE_WRITER, null));
			if (fs == null) {
				return;
			}
			((FileWriterSource) fs).write(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	class CompletePluginCollector extends PluginCollector {

		public CompletePluginCollector(Collection list) {
			super(list);
		}
		@Override
		public void objectWritten (Object object)
		{
			Class cls = object.getClass();
			while (cls != null) {
				ClassLoader l = cls.getClassLoader ();
				if (l instanceof PluginClassLoader)
				{
					list.add (((PluginClassLoader) l).getPluginDescriptor ());
				}
				cls = cls.getSuperclass();
			}
		}
	}
}
