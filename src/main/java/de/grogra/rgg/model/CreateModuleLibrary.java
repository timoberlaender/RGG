package de.grogra.rgg.model;

import de.grogra.pf.boot.Main;
import de.grogra.pf.io.FileWriterSource;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemVisitor;
import de.grogra.pf.registry.PluginClassLoader;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.registry.Value;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.FileChooserResult;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.SourceDirectory;
import de.grogra.pf.ui.registry.SourceFile;
import de.grogra.reflect.ClassAdapter;
import de.grogra.reflect.Type;
import de.grogra.util.IOWrapException;
import de.grogra.util.MimeType;
import de.grogra.util.Utils;
import de.grogra.vfs.FileSystem;
import de.grogra.vfs.LocalFileSystem;
import de.grogra.vfs.MemoryFileSystem;
import de.grogra.xl.compiler.BytecodeWriter;
import de.grogra.xl.compiler.CClass;
import de.grogra.xl.compiler.CompilationUnit;
import de.grogra.xl.compiler.Compiler;
import de.grogra.xl.compiler.CompilerOptions;
import de.grogra.xl.compiler.scope.ClassPath;
import de.grogra.xl.compiler.scope.CompilationUnitScope;
import de.grogra.xl.compiler.scope.PackageImportOnDemand;
import de.grogra.xl.compiler.scope.Scope;
import de.grogra.xl.compiler.scope.SingleStaticImport;
import de.grogra.xl.compiler.scope.SingleTypeImport;
import de.grogra.xl.compiler.scope.StaticImportOnDemand;
import de.grogra.xl.compiler.scope.TypeImportOnDemand;
import de.grogra.xl.util.ObjectList;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import de.grogra.graph.impl.Node;

/**
 * A filter used to recompile the sources with a different default package. 
 * The newly compiled sources are exported into a usable lib.
 */
public class CreateModuleLibrary implements RegistryContext {

	public static final IOFlavor EXPORT_TO_LIB = IOFlavor.valueOf (CompiledLib.class);

	protected CompilationUnitScope compilationUnit;
	protected final ObjectList<FilterSource> sources = new ObjectList<FilterSource> ();

	static String pkgName;
	static String pluginName;
	Registry reg;
	static Set imports;
	
	public CreateModuleLibrary(Registry r) {
		reg=r;
	}

	public Registry getRegistry() {
		return reg;
	}

	
	public static void exportCompiled (Item item, Object info, Context ctx)
	{
		boolean bkpcreated = false;
		File bkp = null;
		pkgName = ctx.getWindow().showInputDialog("Enter package name", 
				"Provide a package name for the export. \n The name should be of the type de.grogra.pkg", null);
		if (pkgName == null || pkgName.isEmpty() || !isCorrectPackageName(pkgName)) {
			ctx.getWindow().showDialog("Export project to Lib",
					"Failed :O", Window.PLAIN_MESSAGE);
			return;
		}
		FileSystem backupfs = null;
		try {
			//create save for backup
			String cwds = (String) ctx.getWorkbench(). getProperty(Workbench.CURRENT_DIRECTORY);
			File cwd = (cwds != null) ? new File(cwds) : null;
			FileChooserResult fr = ctx.getWindow().chooseDirectory("Select a directory", cwd, Window.SAVE_FILE, false);
			if (fr.file.exists() && !fr.file.isDirectory()) {
				return;
			}
			if (!fr.file.exists()) {
				fr.file.mkdirs();
			}
			pluginName = fr.file.getName();
			bkp = new File( fr.file, "backup");
			if (!bkp.exists()) {
				bkp.mkdirs();
				bkpcreated=true;
			}
			backupfs = ctx.getWorkbench().getRegistry().getFileSystem();

			Registry r = ctx.getWorkbench().getRegistry();
			r.substituteFileSystem(new LocalFileSystem("pfs", bkp));

			imports = new HashSet<>();
			
			getJar(r);
			Item i = r.getItem("/export/compiled/rggc");
			if (!(i instanceof Value))
			{
				ctx.getWindow().showDialog("Export project to Lib",
						"Failed :O", Window.PLAIN_MESSAGE);
				return;
			}

			if (fr != null)
			{
				Object object = new CompiledLib(r, pkgName, imports);
				FilterSource fs = IO.createPipeline(
						new ObjectSourceImpl(object, "registry", EXPORT_TO_LIB, r, null),
						new IOFlavor(new MimeType("application/octet-stream"), IOFlavor.FILE_WRITER, null));
				if (fs == null) {
					ctx.getWindow().showDialog("Export project to Lib",
							"Failed :O", Window.PLAIN_MESSAGE);
					return;
				}
				((FileWriterSource) fs).write(fr.file);
			}
		}
		catch (IOException e) {
			ctx.getWindow().showDialog("Export project to Lib",
					"Failed :O", Window.PLAIN_MESSAGE);
		}
		finally {
			if (backupfs != null) {
				ctx.getWorkbench().getRegistry().restoreFileSystem(backupfs);
			}
			if (bkpcreated && bkp !=null) {
				deleteFolder(bkp);
			}
			Item i = ctx.getWorkbench().getRegistry().getItem("/export/compiled/rggc");
			if (i instanceof Value)
			{
				i.deactivate();
				i.remove();
			}
		}
	}
	
	/**
	 * Test if the given string is of type IDENTIFIER (DOT IDENTIFIER)*
	 */
	public static boolean isCorrectPackageName(String s) {
		return s.matches("[A-z0-9À-ú_]+(\\.[A-z0-9À-ú_]+)*");
	}
	
	/**
	 * Recursively delete a folder.
	 */
	public static void deleteFolder(File folder) {
	    File[] files = folder.listFiles();
	    if(files!=null) { //some JVMs return null for empty dirs
	        for(File f: files) {
	            if(f.isDirectory()) {
	                deleteFolder(f);
	            } else {
	                f.delete();
	            }
	        }
	    }
	    folder.delete();
	}


	public static void getJar (Registry reg)
	{
		Node i = reg.getDirectory("/project/objects/files", null);
		if (i== null) {
			return;
		}
		SourceFile sf=null;
		while( i != null) {
			if ((i=i.getBranch()) instanceof SourceFile) {
				sf = (SourceFile)i;
				break;
			} 
		}
		if (sf == null) {
			return;
		}

		CreateModuleLibrary rl = new CreateModuleLibrary(reg);

		try
		{
			Item root =  ((Item) sf.getAxisParent ());
			if (root instanceof SourceDirectory)
				root = ((SourceDirectory) root).getResourceDirectory();
			root.forAll (null, null, new ItemVisitor ()
			{
				public void visit (Item item, Object info)
				{
					if ((item instanceof SourceFile))
					{
						if ((((SourceFile)item).getMimeType().getMediaType().contentEquals("text/x-grogra-rgg") )) {
							Object file = reg.getFileSystem().getFile( IO.toPath( ((SourceFile) item).getSystemId() ) );
							wrapReferences(reg.getFileSystem(), file);
						}
						rl.addResource (((SourceFile) item).toFileSource ());
					}
				}
			}, null, false);
			rl.loadResource (sf.getRegistry ());
		}
		catch (Exception e)
		{
		}
	}

	public void loadResource (Registry r) throws IOException
	{
		MemoryFileSystem fs = new MemoryFileSystem("export_classes");
		Type<?>[] a = compile (null, null, fs);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		OutputStream out = new BufferedOutputStream (bout);
		fs.writeJar (out);
		out.flush ();
		out.close ();
		// the rgg method toolbar look for ALL TypeItem. not only in /compiled. Thus, this duplicate the methods.
//		for (int i = 0; i < a.length; i++)
//		{
//			r.getDirectory ("/export", null).add (new TypeItem (a[i]));
//		}
		r.getDirectory ("/export/compiled", null).add (new Value("rggc", bout.toByteArray()));
	}

	/**
	 * Basically the same as the parent class. But it adds the compiler option -> default pacakge
	 */
	private Type<?>[] compile (CClass shell, ClassLoader parentLoader, MemoryFileSystem fs) throws IOException
	{
		ObjectList<CompilationUnit> sourceList = new ObjectList<CompilationUnit> ();
		ObjectList<CompilationUnit> classList = new ObjectList<CompilationUnit> ();
		XLFilter.CLASSPATH.set (XLFilter.createDefaultClassPath (this));
		StringBuffer names = new StringBuffer();
		boolean short_names;
		int nb_files=0;
		try {
			Item opt = Item.resolveItem (Workbench.current(), "/rgg/compilation");
			short_names = Utils.getBoolean (opt, "log_short_names", false);
		}catch (NullPointerException e) {short_names=false;}

		try
		{
			for (int i = 0; i < sources.size (); i++)
			{
				ObjectSource s =  (ObjectSource) sources.get (i);
				CompilationUnit u = (CompilationUnit) s.getObject ();
				if (u.jarBytes != null)
				{
					classList.add (u);
				}
				else
				{
					sourceList.add (u);
					if (u.options!=null) {
						u.options.defaultpackagename = pkgName;
					}
					if (short_names && nb_files >=2) {
					} else {
						if (names.length() > 0)
						{
							names.append (", ");
						}
						names.append (s.getSystemId ());
					}
					nb_files++;
				}
			}
		}
		finally
		{
			XLFilter.CLASSPATH.set (null);
		}
		CompilationUnit[] src = sourceList.toArray (new CompilationUnit[sourceList.size ()]);
		Compiler c = new Compiler ();
		CompilationUnitScope[] cus = c.compile (src, shell, null, null, null, true);

		if (cus.length > 0)
		{
			compilationUnit = cus[0];
		}
		int version = org.objectweb.asm.Opcodes.V11;
		CompilerOptions opts = null;
		if (src.length > 0)
		{
			opts = src[0].options;
			for (int i = 0; i < src.length; i++)
			{
				if (!src[i].options.supportsVersion (version))
				{
					opts = src[i].options;
					version = opts.javaVersion;
				}
				c.problems.addAll (src[i].problems);
			}
		}
		else
		{
			opts = new CompilerOptions();
		}
		ObjectList<Type<?>> types = new ObjectList<Type<?>> ();
		for (int j = 0; j < cus.length; j++)
		{
			Type<?>[] a = cus[j].getDeclaredTypes ();
			for (int k = 0; k < a.length; k++)
			{
				add (types, a[k]);
			}
		}
		for (int i = 0; i < classList.size(); i++)
		{
			fs.readJar(new ByteArrayInputStream(classList.get(i).jarBytes), false);
		}
		if (parentLoader == null)
		{
			if (compilationUnit != null)
			{
				parentLoader = ClassPath.get (compilationUnit).getClassLoader ();
			}
			else
			{
				parentLoader = XLFilter.getLoaderForRegistry (this).getClassLoader();
			}
		}
		addImportsToList(cus);
		BytecodeWriter w = new BytecodeWriter (opts, parentLoader, types);
		for (CompilationUnitScope cs : cus)
		{
			w.write (cs, fs, fs.getRoot ());
		}
		ClassAdapter.URLClassLoaderWithPool loader = new ClassAdapter.URLClassLoaderWithPool (
				new URL[] {fs.toURL (fs.getRoot ())}, parentLoader);


		ObjectList<Type<?>> compiledTypes = new ObjectList<Type<?>>();
		ObjectList<Object> dirStack = new ObjectList<Object>();
		dirStack.push(fs.getRoot());
		while (!dirStack.isEmpty())
		{
			Object dir = dirStack.pop();
			int dc = fs.getChildCount(dir);
			for (int i = 0; i < dc; i++)
			{
				Object child = fs.getChild(dir, i);
				if (fs.isLeaf(child))
				{
					String name = fs.getPath(child);
					if (name.endsWith(".class"))
					{
						name = name.substring(0, name.length() - 6).replace('/', '.');
						try
						{
							compiledTypes.push(ClassAdapter.wrap (Class.forName (name, false, loader), loader));
						}
						catch (ClassNotFoundException e)
						{
							throw new IOWrapException (e);
						}
					}
				}
				else
				{
					dirStack.push(child);
				}
			}
		}
		return compiledTypes.toArray(new Type<?>[compiledTypes.size()]);
	}

	public boolean addResource (FilterSource source)
	{
		FilterSource cu = IO.createPipeline (source, CompilationFilter.CUNIT_FLAVOR);
		if (cu == null)
		{
			return false;
		}
		sources.add (cu);
		return true;
	}

	protected static void add (ObjectList<Type<?>> list, Type<?> type)
	{
		list.add (type);
		for (int i = type.getDeclaredTypeCount () - 1; i >= 0; i--)
		{
			add (list, type.getDeclaredType (i));
		}
	}

	protected static void wrapReferences(FileSystem fs, Object file) {
		if ( fs.isDirectory(file)) {
			return;
		}
		if (file instanceof File) {
			try (FileWriter fw = new FileWriter((File)file, true);
				    BufferedWriter bw = new BufferedWriter(fw)) {
				bw.write(ReferenceWrapper.getWrapper("plg:"+pkgName + "." + pluginName+"/"));
				bw.newLine();
			} catch(IOException e) {}
		} else if (file != null) { // mem.entry
			//TODO? 
			// Should not happen - the filesystem should be local.
		}
		return;
	}
	
	
	protected void addImportsToList(CompilationUnitScope[] cus) {
		for (CompilationUnitScope cs : cus) {
			Scope s = cs.getEnclosingScope();
			while (s != null) {
				try {
					ClassLoader cl = null;
					// all import scopes are tested -> if from plugin add them
					if (s instanceof SingleTypeImport) {
						cl = ((SingleTypeImport)s).getImportedType().getImplementationClass().getClassLoader();
					} else if (s instanceof SingleStaticImport) {
						cl = ((SingleStaticImport)s).getImportedType().getImplementationClass().getClassLoader();
					}else if (s instanceof PackageImportOnDemand) {
						Type p = ((PackageImportOnDemand)s).getImportedPackage().getOneType();
						cl = (p != null)? p.getImplementationClass().getClassLoader() : null;
					}else if (s instanceof StaticImportOnDemand) {
						cl = ((StaticImportOnDemand)s).getImportedType().getImplementationClass().getClassLoader();
					} else if (s instanceof TypeImportOnDemand) {
						cl = ((TypeImportOnDemand)s).getImportedType().getImplementationClass().getClassLoader();
					}
					if (cl instanceof PluginClassLoader)
					{
						imports.add (((PluginClassLoader) cl).getPluginDescriptor ());
					}
				} catch(Exception e) {
				}
				s = s.getEnclosingScope();
			}
		}
		if (imports.contains(Main.getRegistry().getPluginDescriptor("de.grogra"))) {
			imports.remove(Main.getRegistry().getPluginDescriptor("de.grogra"));
		}
	}
	
}
