package de.grogra.rgg.model;

/**
 * Return a string to inject in each file to override the ref classes and library methods.
 * It simply adds the prefix string to the Ref creations. 
 */
public class ReferenceWrapper {

	public static String getWrapper(String prefix) {
		return "\n //The following lines where automatically added. They should have been deleted. \n"
				+ "//They should NOT be in your file.\n"
				+ "static de.grogra.rgg.FunctionRef function(String s){\n"
				+ "	return de.grogra.rgg.Library.function(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class FunctionRef extends de.grogra.rgg.FunctionRef {\n"
				+ "	FunctionRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.rgg.CurveRef curve(String s){\n"
				+ "	return de.grogra.rgg.Library.curve(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class CurveRef extends de.grogra.rgg.CurveRef {\n"
				+ "	CurveRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.rgg.SurfaceRef surface(String s){\n"
				+ "	return de.grogra.rgg.Library.surface(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class SurfaceRef extends de.grogra.rgg.SurfaceRef {\n"
				+ "	SurfaceRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.pf.data.DatasetRef dataset(String s){\n"
				+ "	return de.grogra.rgg.Library.dataset(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class DatasetRef extends de.grogra.pf.data.DatasetRef {\n"
				+ "	DatasetRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.imp3d.shading.ShaderRef shader(String s){\n"
				+ "	return de.grogra.rgg.Library.shader(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class ShaderRef extends de.grogra.imp3d.shading.ShaderRef {\n"
				+ "	ShaderRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.imp.objects.ImageRef image(String s){\n"
				+ "	return de.grogra.rgg.Library.image(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class ImageRef extends de.grogra.imp.objects.ImageRef {\n"
				+ "	ImageRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.gpuflux.imp3d.objects.SpectrumRef spectrum(String s){\n"
				+ "	return de.grogra.rgg.Library.spectrum(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class SpectrumRef extends de.grogra.gpuflux.imp3d.objects.SpectrumRef {\n"
				+ "	SpectrumRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "\n"
				+ "static de.grogra.gpuflux.imp3d.objects.LightDistributionRef light(String s){\n"
				+ "	return de.grogra.rgg.Library.light(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class LightDistributionRef extends de.grogra.gpuflux.imp3d.objects.LightDistributionRef {\n"
				+ "	LightDistributionRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "@SuppressWarnings(\"deprecation\")\n"
				+ "static de.grogra.imp3d.shading.MaterialRef material(String s){\n"
				+ "	return de.grogra.rgg.Library.material(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "@SuppressWarnings(\"deprecation\")\n"
				+ "class MaterialRef extends de.grogra.imp3d.shading.MaterialRef {\n"
				+ "	MaterialRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.rgg.FileRef file(String s){\n"
				+ "	return de.grogra.rgg.Library.file(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class FileRef extends de.grogra.rgg.FileRef {\n"
				+ "	FileRef(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "static de.grogra.rgg.Reference reference(String s){\n"
				+ "	return de.grogra.rgg.Library.reference(\""+prefix+"\"+s);\n"
				+ "}\n"
				+ "class Reference extends de.grogra.rgg.Reference {\n"
				+ "	Reference(String s){\n"
				+ "		super(\""+prefix+"\"+s);\n"
				+ "	}\n"
				+ "}\n"
				+ "//End of automated text.";
	}
}
