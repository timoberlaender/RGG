package de.grogra.rgg.parallel;

import de.grogra.rgg.ConcurrentTasks;
import de.grogra.rgg.model.RGGProducer;
import de.grogra.xl.query.QueryState;

public class ParallelProducer extends RGGProducer{

	private transient ConcurrentTasks tasks = new ConcurrentTasks ();
	
	
	public ParallelProducer(QueryState match) {
		super(match);
	}

	@Override
	public boolean producer$beginExecution (int arrow)
	{
		return super.producer$beginExecution (arrow);
	}

	@Override
	public void producer$endExecution (boolean applied)
	{
		super.producer$endExecution (applied);
	}
}
