package de.grogra.rgg;

import de.grogra.xl.lang.*;

/*
 * A extension of the de.grogra.xl.util.Operators class to 
 * support the omitnan argument. The class is statically 
 * imported by default with the rggfilter.
 * The operators only consider double or float arguments
 */
public final class NumOperators
{
	public static final String OMITNAN = "omitnan";

	private NumOperators ()
	{
	}
	
	private static boolean omitNan(String arg) {
		return arg.equalsIgnoreCase(OMITNAN);
	}

	/*
	 * -----------------------------------------------------------
	 * 					Min & Max
	 * -----------------------------------------------------------
	 */

	public static void min (Aggregate a, float value, String arg)
	{
		if (a.initialize ())
		{
			a.fval = Float.POSITIVE_INFINITY;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Float.isNaN(value))) {
				a.fval = Math.min (a.fval, value);
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.fval= Float.NaN;
			}
		}
	}

	public static void max (Aggregate a, float value, String arg)
	{
		if (a.initialize ())
		{
			a.fval = Float.NEGATIVE_INFINITY;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Float.isNaN(value))) {
				a.fval = Math.max (a.fval, value);
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.fval= Float.NaN;
			}
		}
	}
	
	public static void min (Aggregate a, double value, String arg)
	{
		if (a.initialize ())
		{
			a.dval = Double.POSITIVE_INFINITY;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Double.isNaN(value))) {
				a.dval = Math.min (a.dval, value);
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.dval= Double.NaN;
			}
		}
	}

	public static void max (Aggregate a, double value, String arg)
	{
		if (a.initialize ())
		{
			a.dval = Double.NEGATIVE_INFINITY;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Double.isNaN(value))) {
				a.dval = Math.max (a.dval, value);
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.dval= Double.NaN;
			}
		}
	}
	
	/*
	 * -----------------------------------------------------------
	 * 					Sum, Mean, prod
	 * -----------------------------------------------------------
	 */
	public static void sum (Aggregate a, float value, String arg)
	{
		if (a.initialize ())
		{
			a.fval = 0;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Float.isNaN(value))) {
				a.fval += value;
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.fval= Float.NaN;
			}
		}
	}

	public static void mean (Aggregate a, float value, String arg)
	{
		if (a.initialize ())
		{
			a.fval = 0;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Float.isNaN(value))) {
				a.fval += value;
				a.ival1++;
			}
		}
		else
		{
			a.fval /= a.ival1;
		}
	}

	public static void prod (Aggregate a, float value, String arg)
	{
		if (a.initialize ())
		{
			a.fval = 1;
			a.ival1 = 0;
			
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Float.isNaN(value))) {
				a.fval *= value;
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.fval= Float.NaN;
			}
		}
	}

	public static void sum (Aggregate a, double value, String arg)
	{
		if (a.initialize ())
		{
			a.dval = 0;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Double.isNaN(value))) {
				a.dval += value;
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.dval= Double.NaN;
			}
		}
	}

	public static void mean (Aggregate a, double value, String arg)
	{
		if (a.initialize ())
		{
			a.dval = 0;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Double.isNaN(value))) {
				a.dval += value;
				a.ival1++;
			}
		}
		else
		{
			a.dval /= a.ival1;
		}
	}

	public static void prod (Aggregate a, double value, String arg)
	{
		if (a.initialize ())
		{
			a.dval = 1;
			a.ival1 = 0;
		}
		if (!a.isFinished ())
		{
			if (!(omitNan(arg) && Double.isNaN(value))) {
				a.dval *= value;
				a.ival1++;
			}
		}
		else {
			if (a.ival1==0) {
				a.dval= Double.NaN;
			}
		}
	}
}
