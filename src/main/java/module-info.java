module rgg {
	exports de.grogra.stl;
	exports de.grogra.ply;
	exports de.grogra.obj;
	exports de.grogra.tex;
	exports de.grogra.grogra;
	exports de.grogra.rgg.model;
	exports de.grogra.turtle;
	exports de.grogra.webgl;
	exports de.grogra.chem;
	exports de.grogra.mtg;
	exports de.grogra.rgg.numeric;
	exports de.grogra.rgg.parallel;
	exports de.grogra.rgg;
	
	requires gpuFlux;
	requires grammar;
	requires graph;
	requires imp;
	requires imp3d;
	requires math;
	requires numeric;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.compiler;
	requires xl.core;
	requires xl.impl;
	requires xl.vmx;
	requires antlr;
	requires commons.math3;
	requires java.datatransfer;
	requires java.desktop;
	requires java.logging;
	requires java.xml;
	requires java.base;
	requires org.objectweb.asm;
	
	opens de.grogra.rgg to utilities;
	opens de.grogra.rgg.model to utilities,platform;
}