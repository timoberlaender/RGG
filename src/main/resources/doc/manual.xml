<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN" "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<book id="groimp-manual">
	<title>RGG Plugin Manual</title>
	<bookinfo>
		<title>RGG Plugin Manual</title>
		<pubdate>2006</pubdate>
		<copyright>
			<year>2006</year>
<holder>Lehrstuhl Grafische Systeme,
Brandenburgische Technische Universität Cottbus</holder>
		</copyright>
	</bookinfo>

<chapter><title>GroIMP's Relational Growth Grammars Plugin</title>

<section><title>Overview</title>

<para><emphasis>Growth grammars</emphasis> are a rule-based approach to the
modelling of dynamic systems. In contrast to conventional imperative
programming languages, rule-based approaches provide a natural and concise way
of modelling: Most systems to be modeled behave according to a set of rules,
and our perception works this way and not imperatively using a list of
operations which have to be processed step by step.</para><para>Relational
growths grammars (RGG) are part of the current research at the
<ulink url="http://www-gs.informatik.tu-cottbus.de/">Chair for
Practical Computer Science / Graphics Systems</ulink>
at the Brandenburg University of Technology
Cottbus (Germany) and its cooperation partners. They are defined as a
unification of L-systems and graph grammars.
This user manual explains how relational growths grammars are used within
GroIMP, more information about relational
growth grammars can be found at the web page
<ulink url="http://www.grogra.de/">www.grogra.de</ulink>, more information
about the programming language XL, which is used within GroIMP
as an implementation of the calculus of relational growth grammars,
can be found in the
<ulink url="plugin:de.grogra.xl/doc/index.html">XL
language speficication</ulink>.
</para>

</section>

<section><title>Opening an XL File</title>

<para>GroIMP understands relational growth grammars written in the language XL,
which is based on Java, see
<ulink url="http://www.grogra.de/">www.grogra.de</ulink>
and the <ulink url="plugin:de.grogra.xl/doc/index.html">XL
language speficication</ulink>. At the web page,
there exist some example XL files,
which you should download in order to have a starting
point. Or, if you have installed the Examples-plugin, just select the menu
<guimenuitem>File/Show Examples</guimenuitem>.
A simple, classical example is the Koch curve whose XL source is shown
here, you may create the file <filename>Koch.xl</filename> by
Copy&amp;Paste.</para>
<programlisting>import de.grogra.rgg.*;
import de.grogra.lsystem.*;

public class Koch extends RGG {
    public void derivation() [
        Axiom ==&gt; F(10) RU(120) F(10) RU(120) F(10);
        F(x) ==&gt; F(x/3) RU(-60) F(x/3) RU(120) F(x/3) RU(-60) F(x/3);
    ]
}
</programlisting><para>You can open an XL file like the one shown file via the
menu item <guimenuitem>File/Open</guimenuitem>, this creates a new project
containing the XL file. For a good RGG modelling workflow, you should switch to
the RGG panel layout in the menu <guisubmenu>Panels/Set
Layout</guisubmenu>. This brings up the most frequently used panels: RGG
toolbar, 3D view, text editor, attribute editor, file exporer, meta object
browser, message panel, and XL console, see
<xref linkend="f-rgg-panels"/>.</para>

<figure id="f-rgg-panels"><title>Panels in the RGG Layout</title>
<graphic format="PNG" fileref="res/panels.png"/></figure>

<para>The meta object browser displays objects which have an influence on the
scene, but which are not part of the material scene. The RGG object is an
example of such an object, and if you open an XL file containing an RGG class,
an instance of this class will be created automatically (if the file can be
compiled successfully) and inserted into the list of meta objects. By a double
click on the RGG object, its editable attributes
(the <structname>public</structname> fields which are declared in its class
and are annotated by <classname>de.grogra.annotation.Editable</classname>)
are shown in the attribute editor.</para></section>

<section><title>Working with a Relational Growth Grammar</title>

<para>After an RGG object has been loaded successfully from an XL file, its
<structname>public</structname> methods are made available in the RGG
toolbar. Each method appears twice: In the list prefixed with
<guibutton>Apply</guibutton>, a click invokes the method once, in the list
prefixed with <guibutton>Run</guibutton>, a click starts the repeated
invocation of the method. To stop this loop, click the
<guibutton>Stop</guibutton> button in the toolbar. Initially, buttons for the
first method are accessible immediately; to see a button list for the other
methods, you have to click on the small arrow at the right button
border.</para><para>Normally, RGG methods cause the application of graph
grammar rules to the scene. This is the case for the examples provided at
GroIMP's web page, and you should try these examples to get familiar with
GroIMP's RGG facilities.</para><para>The XL source code of a relational growth
grammar can be edited in GroIMP's internal text editor, just double-click on
the XL file in the file explorer. Whenever modifications of an XL file are
saved using the text editor's <guibutton>Save</guibutton> button, it is
recompiled, and the RGG object becomes reinitialized. Compilation errors are
shown in the message panel and contain hypertext links to their source
code locations.</para>
<para>If the filename ends in <filename>.rgg</filename> instead of
<filename>.xl</filename>, a simplified syntax is used. In this case,
the most common import statements are implicit, as is the
enclosing <classname>RGG</classname>-class declaration.
</para>
</section>

<section><title>Opening an L-Sytem in GROGRA-Syntax</title>

<para>GroIMP's RGG Plugin contains an import filter for
non-sensitive GROGRA L-systems (<filename>*.lsy</filename>-files). They can be
opened and edited as for XL files. However, not all features of GROGRA are
supported yet.</para></section>
</chapter>

</book>
