# RGG

The RGG plugin establishes the link between GroIMP and the XL programming language. It uses the base implementation of the XL interfaces to make the graph of GroIMP accessible to the run-time system of the XL programming language and to provide a derivation mechanism which is compliant with relational growth grammars. This plugin also provides a re-implementation of some of the features of the GROGRA software within the new framework.

It includes support of the file types :
- ".java" 
- ".xl"
- ".rgg"
- ".lsy/ .ssy"
- ".rggc" (compiled rgg)
- ".dtd"
- ".dtg"
- ".mtg" (shouldn't be there...)
- ".stl"
- ".ply" (as graph object for export)
- ".obj" (as graph object for export)
- ".tex" (both as text and export)
- ".html" (for view and export)
